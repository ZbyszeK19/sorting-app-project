package sortingapplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;



@RunWith(Parameterized.class)
public class SortingApplicationTest {
    private final String[] arrayTest;
    private final int[] arrayAssert;
    int length;
    protected SortingApplication sortingApplication=new SortingApplication();

    public SortingApplicationTest(String[] arrayTest,int[] arrayAssert,int length){
        this.arrayTest=arrayTest;
        this.arrayAssert=arrayAssert;
        this.length=length;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
            String[] array1={"1","2","3","4","5","6","7","8","9","10"};
            String[] array2={"1"};
            String[] array3={"1","4","7","3","1","7","4"};
            String[] array4={"24","17","3","45","100","1000","15"};
            String[] array5={};
            String[] array6={"15","3","10"};
            String[] array7={"-15","-14","14","-15","333","12"};
            String[] array8={"54","19","16","22","1256","0","-3","-4","0"};

            int[] arrayResult1={1,2,3,4,5,6,7,8,9,10};
            int[] arrayResult2={1};
            int[] arrayResult3={1,1,3,4,4,7,7};
            int[] arrayResult4={3,15,17,24,45,100,1000};
            int[] arrayResult5={};
            int[] arrayResult6={3,10,15};
            int[] arrayResult7={-15,-15,-14,12,14,333};
            int[] arrayResult8={-4,-3,0,0,16,19,22,54,1256};
        return asList(
                new Object[]{array1,arrayResult1,array1.length},
                new Object[]{array2,arrayResult2,array2.length},
                new Object[]{array3,arrayResult3,array3.length},
                new Object[]{array4,arrayResult4,array4.length},
                new Object[]{array5,arrayResult5,array5.length},
                new Object[]{array6,arrayResult6,array6.length},
                new Object[]{array7,arrayResult7,array7.length},
                new Object[]{array8,arrayResult8,array8.length}
                );
    }

    @Test()
    public void sorting() {
        assertArrayEquals(sortingApplication.sorting(arrayTest,length),arrayAssert);
    }
}