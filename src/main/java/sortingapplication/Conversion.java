package sortingapplication;

/**
 * A class that converts String to int
 */
public class Conversion {
    /**
     *
     * @param table Given numbers by user
     * @param length Number of numbers
     * @return  Strings converts to int
     */
    public int [] toInt(String[] table,int length){
        int[] arr1 = new int[length];
        try {
            for (int i = 0; i < length; i++) {
                arr1[i] = Integer.parseInt(table[i]);
            }
        }catch(NumberFormatException e){
            arr1=null;
            System.out.println("Use only numbers\n");
        }
        return arr1;
    }
}
