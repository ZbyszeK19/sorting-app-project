package sortingapplication;

/**
 * Start of the application
 */
public class Main {
    /**
     * The main method
     * @param args A field for numbers
     */
    public static void main(String[] args) {
        int length = args.length;
        SortingApplication sortingApplication = new SortingApplication();
        sortingApplication.sorting(args,length);
    }
}
