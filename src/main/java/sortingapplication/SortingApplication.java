package sortingapplication;

/**
 * Here is the sorting class
 */
public class SortingApplication {
    /**
     *
     * @param table Given numbers by user
     * @param length Number of numbers
     * @return Sorted numbers
     */
    public int[] sorting(String table[],int length){
        if(length>10){
            throw new IllegalArgumentException("Too many arguments");
        }
        Conversion conversion = new Conversion();
        int[] arr1 = conversion.toInt(table,length);
        int k,temp;
        for (int m=length; m >=0; m--) {
            for (int i = 0; i < length-1; i++) {
                k=i+1;
                if(arr1[i]>arr1[k]){
                    temp=arr1[k];
                    arr1[k]=arr1[i];
                    arr1[i]=temp;
                }
            }
        }
        for (int variable:arr1) {
            System.out.println(variable);
        }
        return arr1;
    }
}
